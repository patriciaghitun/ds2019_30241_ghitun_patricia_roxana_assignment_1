package com.example.springdemo.entities.medication;

import javax.persistence.*;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_per_plan")

public class MedicationPerPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "intakemoment", length = 200)
    private String intakemoment;

    @ManyToOne
    @JoinColumn(name = "medication_id")
    private Medication medication;

    @ManyToOne
    @JoinColumn(name = "medication_plan_id")
    private MedicationPlan medicationPlan;


    public MedicationPerPlan() {}

    public MedicationPerPlan(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakemoment() {
        return intakemoment;
    }

    public void setIntakemoment(String intakemoment) {
        this.intakemoment = intakemoment;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}