package com.example.springdemo.dto.person;
import com.example.springdemo.entities.person.User;

public class PatientDTO {
    private Integer id;
    private String medicalRecord;
    private User user;
    private User caregiver;
    private User doctor;

    public PatientDTO() {}

    public PatientDTO(Integer id, String medicalRecord, User user, User caregiver, User doctor) {
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.user = user;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }
}
