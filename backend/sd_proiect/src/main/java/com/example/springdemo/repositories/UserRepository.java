package com.example.springdemo.repositories;

import com.example.springdemo.entities.person.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    @Query(value = "SELECT u " +
            "FROM User u " +
            "ORDER BY u.id")
    List<User> getAllOrdered(); // by id

    //1 = caregiver role
    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.role = 1 " +
            "ORDER BY u.id")

    List<User> getAllCaregiversOrdered(); // by id


}
