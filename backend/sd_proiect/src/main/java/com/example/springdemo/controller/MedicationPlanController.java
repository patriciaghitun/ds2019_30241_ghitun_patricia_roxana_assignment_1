package com.example.springdemo.controller;


import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication_plan")
public class MedicationPlanController {

    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }


    @GetMapping(value = "/{id}")
    public MedicationPlanDTO findById(@PathVariable("id") Integer id){
        return medicationPlanService.findMedicationPlanById(id);
    }

    @GetMapping(value = "/all")
    public List<MedicationPlanDTO> findAll(){
        return medicationPlanService.findAll();
    }

    //CRUD
    @PostMapping(value = "/add")
    public Integer insertMedicationPlanDTO(@RequestBody MedicationPlanDTO medicationPlanDTO){
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.update(medicationPlanDTO);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody MedicationPlanDTO medicationPlanDTO){
        medicationPlanService.delete(medicationPlanDTO);
    }
}
