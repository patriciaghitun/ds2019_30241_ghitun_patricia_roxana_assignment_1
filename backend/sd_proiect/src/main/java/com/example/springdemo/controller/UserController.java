package com.example.springdemo.controller;


import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/findById/{id}")
    public UserDTO findById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/findUserById/{id}")
    public UserDTO findUserById(@PathVariable("id") Integer id){
        return userService.findUserById(id);
    }

    @GetMapping(value = "/findByUsername/{username}")
    public UserDTO findByUsername(@PathVariable("username") String userame){
        return userService.findByUsername(userame);
    }

    //CRUD on USER
    @GetMapping(value = "/all")
    public List<UserDTO> findAll(){
        return userService.findAll();
    }

    @PostMapping(value = "/add")
    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateUser(@RequestBody UserDTO userDTO) {
        return userService.update(userDTO);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody UserDTO userDTO){
        userService.delete(userDTO);
    }

    //DOCTOR ROLE
    //CRUD ON PATIENTS
}
