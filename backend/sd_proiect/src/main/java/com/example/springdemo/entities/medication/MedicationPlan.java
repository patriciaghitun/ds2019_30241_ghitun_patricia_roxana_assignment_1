package com.example.springdemo.entities.medication;

import com.example.springdemo.entities.person.Patient;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
public class MedicationPlan implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @NotNull
    @ManyToOne(cascade = CascadeType.REMOVE)//Many medicationPlan to one patient - cascade type remove =>
    // trebuie sa am userul in baza de date inainte ca sa pot adauga un nou medication plan cu patient-ul ala
    @JoinColumn(name = "patient_id")
    private Patient patient; // informatii despre userul respectiv

    //perioada
    @Column(name = "start", length = 200)
    private String start;

    @Column(name = "end", length = 200)
    private String end;

    public MedicationPlan() {}

    public MedicationPlan(Integer id, Patient patient, String start, String end) {
        this.id = id;
        this.patient = patient;
        this.start = start;
        this.end = end;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}