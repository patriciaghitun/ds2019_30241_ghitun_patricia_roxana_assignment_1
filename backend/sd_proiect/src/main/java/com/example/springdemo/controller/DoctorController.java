package com.example.springdemo.controller;

import com.example.springdemo.dto.medication.MedicationDTO;
import com.example.springdemo.dto.medication.MedicationPerPlanDTO;
import com.example.springdemo.dto.medication.MedicationPlanDTO;
import com.example.springdemo.dto.person.PatientDTO;
import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.entities.medication.MedicationPerPlan;
import com.example.springdemo.entities.medication.MedicationPlan;
import com.example.springdemo.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/doctor")
public class DoctorController {

    private final UserService userService;
    private final PatientService patientService;
    private final MedicationService medicationService;
    private final MedicationPlanService medicationPlanService;
    private final MedicationPerPlanService medicationPerPlanService;

    @Autowired
    public DoctorController(UserService userService, PatientService patientService, MedicationService medicationService, MedicationPlanService medicationPlanService, MedicationPerPlanService medicationPerPlanService) {
        this.userService = userService;
        this.patientService = patientService;
        this.medicationService = medicationService;
        this.medicationPlanService = medicationPlanService;
        this.medicationPerPlanService = medicationPerPlanService;
    }

    //CRUD ON PATIENTS

    @GetMapping(value = "/patient/findPatientById/{id}")
    public PatientDTO findPatientById(@PathVariable("id") Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping(value = "/patient/all")
    public List<PatientDTO> findAllPatients(){
        return patientService.findAll();
    }

    @PostMapping(value = "/patient/add")
    public Integer insertPatientDTO(@RequestBody PatientDTO patientDTO){
        return patientService.insert(patientDTO);
    }

    @PutMapping(value = "/patient/update")
    public Integer updateUser(@RequestBody PatientDTO patientDTO) {
        return patientService.update(patientDTO);
    }

    @DeleteMapping(value = "/patient/delete")
    public void delete(@RequestBody PatientDTO patientDTO){
        patientService.delete(patientDTO);
    }

    @DeleteMapping(value = "/patient/deleteById/{id}")
    public void deletePatientById(@PathVariable("id") Integer id){
        patientService.deleteById(id);
    }


    //CRUD ON CAREGIVER - check for caregiver ROLE
    @GetMapping(value = "/caregiver/all")
    public List<UserDTO> findAllCaregivers(){
        return userService.findAllCaregivers();
    }

    @PostMapping(value = "/caregiver/add")
    public Integer insertUserDTO(@RequestBody UserDTO userDTO){
        return userService.insert(userDTO);
    }

    @PutMapping(value = "/caregiver/update")
    public Integer updateUser(@RequestBody UserDTO userDTO) {
        return userService.update(userDTO);
    }

    @DeleteMapping(value = "/caregiver/deleteById/{id}")
    public void deleteCaregiverById(@PathVariable("id") Integer id){
        userService.deleteById(id);
    }

    //CRUD ON MEDICATIONS - merge

    @GetMapping(value = "/medication/all")
    public List<MedicationDTO> findAllMedications(){
        return medicationService.findAll();
    }

    @PostMapping(value = "/medication/add")
    public Integer insertMedicationDTO(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insert(medicationDTO);
    }

    @PutMapping(value = "/medication/update")
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping(value = "/medication/delete")
    public void deleteMedication(@RequestBody MedicationDTO medicationDTO){
        medicationService.delete(medicationDTO);
    }

    @DeleteMapping(value = "medication/deleteById/{id}")
    public void deleteMedicationById(@PathVariable("id") Integer id) {medicationService.deleteById(id);}


    //CREATE MEDICATION PLAN
    @PostMapping(value = "/createMedicationPlan")
    public Integer createMedicationPlan(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        return medicationPlanService.insert(medicationPlanDTO);
    }

    @GetMapping(value = "/getMedicationPlanById/{id}")
    public MedicationPlanDTO getMedicationPlanById(@PathVariable("id") Integer id) {
        return medicationPlanService.findMedicationPlanById(id);
    }

    //ADD MEDICATION TO MEDICATION PLAN
    @PostMapping(value = "/addMedicationToPlan")
    public Integer addMedicationToPlan(@RequestBody MedicationPerPlanDTO medicationPerPlanDTO) {
        return medicationPerPlanService.insert(medicationPerPlanDTO);
    }

    @GetMapping(value = "/getMedicationsForPlan/{id}")
    public List<MedicationPerPlanDTO> getMedicationsForPlan(@PathVariable("id") Integer id) {
        return medicationPerPlanService.getMedicationsForPlan(id);
    }

    @GetMapping(value = "/getMedicationPlanForPatient/{id}")
    public MedicationPlanDTO getMedicationPlanForPatient(@PathVariable("id") Integer id){
        return medicationPlanService.getMedicationPlanForPatient(id);
    }

}
