package com.example.springdemo.repositories;

import com.example.springdemo.entities.medication.Medication;
import com.example.springdemo.entities.medication.MedicationPerPlan;
import com.example.springdemo.entities.medication.MedicationPlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicationPerPlanRepository extends JpaRepository<MedicationPerPlan, Integer> {
    @Query(value = "SELECT u " +
            "FROM MedicationPerPlan u " +
            "ORDER BY u.id")
    List<MedicationPerPlan> getAllOrdered(); // by id

    //List<MedicationPerPlan> getMedicationPerPlanById(Integer id);

    //GET la toate med per plan - lista care au medplan id => toate medicamentele dintr-un med plan
    List<MedicationPerPlan> getMedicationPerPlanByMedicationPlanId(Integer id);

}
