package com.example.springdemo.controller;


import com.example.springdemo.dto.medication.MedicationDTO;
import com.example.springdemo.services.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Integer id){
        return medicationService.findMedicationById(id);
    }

    @GetMapping(value = "/all")
    public List<MedicationDTO> findAll(){
        return medicationService.findAll();
    }

    //CRUD
    @PostMapping(value = "/add")
    public Integer insertMedicationDTO(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insert(medicationDTO);
    }

    @PutMapping(value = "/update")
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO) {
        return medicationService.update(medicationDTO);
    }

    @DeleteMapping(value = "/delete")
    public void delete(@RequestBody MedicationDTO medicationDTO){
        medicationService.delete(medicationDTO);
    }
}
