package com.example.springdemo.services;

import com.example.springdemo.dto.builders.person.UserBuilder;
import com.example.springdemo.dto.person.UserDTO;
import com.example.springdemo.entities.person.User;
import com.example.springdemo.errorhandler.DuplicateEntryException;
import com.example.springdemo.errorhandler.ResourceNotFoundException;
import com.example.springdemo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDTO findUserById(Integer id){
        Optional<User> user  = userRepository.findById(id);

        if (!user.isPresent()) {
            throw new ResourceNotFoundException("User", "user id", id);
        }
        return UserBuilder.generateDTOFromEntity(user.get());
    }

    public UserDTO findByUsername(String username) {
        User user  = userRepository.findByUsername(username);

        return UserBuilder.generateDTOFromEntity(user);
    }

    public List<UserDTO> findAll(){
        List<User> users = userRepository.getAllOrdered();

        return users.stream()
                .map(UserBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }


    public Integer insert(UserDTO userDTO) {
        //sa nu pot insera 2 persoane cu acelasi username!!!
        User user = userRepository.findByUsername(userDTO.getUsername());
        if(user != null){
            throw  new DuplicateEntryException("User", "username", userDTO.getUsername());
        }

        return userRepository
                .save(UserBuilder.generateEntityFromDTO(userDTO))
                .getId();
    }

    public Integer update(UserDTO userDTO) {

        Optional<User> user = userRepository.findById(userDTO.getId());

        if(!user.isPresent()){
            throw new ResourceNotFoundException("User", "user id", userDTO.getId().toString());
        }
        return userRepository.save(UserBuilder.generateEntityFromDTO(userDTO)).getId();
    }

    public void delete(UserDTO userDTO){
        this.userRepository.deleteById(userDTO.getId());
    }

    public void deleteById(Integer id){
        this.userRepository.deleteById(id);
    }

    // DOCTOR ROLE - CRUD ON CAREGIVERS
    public List<UserDTO> findAllCaregivers() {
        List<User> caregivers = userRepository.getAllCaregiversOrdered();

        return caregivers.stream()
                .map(UserBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }
}