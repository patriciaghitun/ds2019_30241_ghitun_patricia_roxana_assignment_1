package com.example.springdemo.dto.medication;

import com.example.springdemo.entities.medication.Medication;
import com.example.springdemo.entities.medication.MedicationPlan;

public class MedicationPerPlanDTO {
    private Integer id;
    private String intakemoment;
    private Medication medication;
    private MedicationPlan medicationPlan;

    public MedicationPerPlanDTO(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakemoment() {
        return intakemoment;
    }

    public void setIntakemoment(String intakemoment) {
        this.intakemoment = intakemoment;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
