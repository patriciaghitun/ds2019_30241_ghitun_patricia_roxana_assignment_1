-- MySQL dump 10.13  Distrib 8.0.18, for macos10.14 (x86_64)
--
-- Host: localhost    Database: proiect_sd
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `medication_per_plan`
--

DROP TABLE IF EXISTS `medication_per_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `medication_per_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `intakemoment` varchar(200) DEFAULT NULL,
  `medication_id` int(11) DEFAULT NULL,
  `medication_plan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKgn2e42ex4j0duqnlu7njv8paj` (`medication_id`),
  KEY `FKfkce1m2eh0v32gp5hp5dxg09c` (`medication_plan_id`),
  CONSTRAINT `FKfkce1m2eh0v32gp5hp5dxg09c` FOREIGN KEY (`medication_plan_id`) REFERENCES `medication_plan` (`id`),
  CONSTRAINT `FKgn2e42ex4j0duqnlu7njv8paj` FOREIGN KEY (`medication_id`) REFERENCES `medication` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_per_plan`
--

LOCK TABLES `medication_per_plan` WRITE;
/*!40000 ALTER TABLE `medication_per_plan` DISABLE KEYS */;
INSERT INTO `medication_per_plan` VALUES (1,'azi',NULL,1),(2,'azi',1,1),(3,'noon',1,6),(4,'evening',2,6),(5,'noon',1,7),(6,'noon',1,8),(7,'azi',1,9),(8,'azi',1,10),(9,'h',1,11),(10,'4',1,12),(11,'g',1,13),(12,'n',1,14),(13,'nn',2,14),(14,'hhu',2,15),(15,'azi la 2 fara 10',1,16),(16,'azi',1,17),(17,'seara',2,18),(18,'azi',2,19),(19,'noon',3,20),(20,'hhh',1,22),(21,'azi',3,24),(22,'test1',1,25),(23,'azi test test 1',2,26),(24,'azi merge',1,26),(25,'med1',1,27),(26,'med2',2,27);
/*!40000 ALTER TABLE `medication_per_plan` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-31 15:24:30
