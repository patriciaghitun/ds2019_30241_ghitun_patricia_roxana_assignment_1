package com.example.sd_android_project.services;

import com.example.sd_android_project.dto.medication.MedicationDTO;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MedicationApi {

    @GET("/medication/all")
    Call<List<MedicationDTO>> getAllMedications();

    @POST("medication/add")
    Call<Integer> insertMedicationDTO(@Body MedicationDTO medicationDTO);
}
