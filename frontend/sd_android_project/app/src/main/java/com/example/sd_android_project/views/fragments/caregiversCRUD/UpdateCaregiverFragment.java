package com.example.sd_android_project.views.fragments.caregiversCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.entities.Role;
import com.example.sd_android_project.entities.User;

public class UpdateCaregiverFragment extends Fragment {

    EditText id, username, password, name, birthDate, gender, address;
    Button update;

    DoctorController doctorController = new DoctorController();
    public static UpdateCaregiverFragment newInstance() {

        Bundle args = new Bundle();

        UpdateCaregiverFragment fragment = new UpdateCaregiverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_caregiver, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id =  view.findViewById(R.id.update_caregiver_id);
        username = view.findViewById(R.id.update_caregiver_username);
        password = view.findViewById(R.id.update_caregivert_password);

        name = view.findViewById(R.id.update_caregiver_name);
        birthDate = view.findViewById(R.id.update_caregiver_birthDate);
        gender = view.findViewById(R.id.update_caregiver_gender);
        address = view.findViewById(R.id.update_caregiver_address);

        update =  view.findViewById(R.id.update_caregiver);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer caregiverId = Integer.parseInt(id.getText().toString().trim());

                UserDTO userDTO = new UserDTO(
                        caregiverId,
                        username.getText().toString().trim(),
                        password.getText().toString().trim(),
                        name.getText().toString().trim(),
                        birthDate.getText().toString().trim(),
                        gender.getText().toString().trim(),
                        address.getText().toString().trim(),
                        Role.CAREGIVER
                );
                updateCaregiver(userDTO);

            }
        });
    }

    private void updateCaregiver(UserDTO caregiverDTO) {
        doctorController.updateCaregiver(this.getContext(), caregiverDTO);
    }
}
