package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

public class Medication {

        @SerializedName("id")
        private Integer id;

        @SerializedName("name")
        private String name;

        @SerializedName("side_effects")
        private String sideEffects;

        @SerializedName("dosage")
        private Integer dosage; // dozaj

        //Medication - MedicationPerPlan

        public Medication(){}

        public Medication(Integer id, String name, String sideEffects, Integer dosage) {
            this.id = id;
            this.name = name;
            this.sideEffects = sideEffects;
            this.dosage = dosage;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSideEffects() {
            return sideEffects;
        }

        public void setSideEffects(String sideEffects) {
            this.sideEffects = sideEffects;
        }

        public Integer getDosage() {
            return dosage;
        }

        public void setDosage(Integer dosage) {
            this.dosage = dosage;
        }
}