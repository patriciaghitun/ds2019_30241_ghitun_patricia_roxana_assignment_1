package com.example.sd_android_project.controller;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.builders.MedicationBuilder;
import com.example.sd_android_project.dto.builders.MedicationPlanBuilder;
import com.example.sd_android_project.dto.builders.PatientBuilder;
import com.example.sd_android_project.dto.builders.UserBuilder;
import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.dto.medication.MedicationPerPlanDTO;
import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.entities.Patient;
import com.example.sd_android_project.entities.Role;
import com.example.sd_android_project.services.DoctorService;
import com.example.sd_android_project.services.UserService;
import com.example.sd_android_project.views.activities.AddMedicationPlan;
import com.example.sd_android_project.views.activities.AddMedicationToPlan;
import com.example.sd_android_project.views.adapters.CaregiverAdapter;
import com.example.sd_android_project.views.adapters.MPMedicationAdapter;
import com.example.sd_android_project.views.adapters.MedicationAdapter;
import com.example.sd_android_project.views.adapters.PatientAdapter;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class DoctorController {

    private static final String TAG = "DoctorController";

    //CRUD ON PATIENTS
    public void addNewPatient(final Context context, final UserDTO userDTO, final String medicalRecord,
                              Integer caregiverId, final String doctorUsername) {

        //TODO:
        //1. Find caregiver dto by id
        //2. Find doctor dto by username
        //3. make a patientdto and add it

        System.out.println("BDAY: "+userDTO.getBirthdate());
        Call<UserDTO> user = UserService.getInstance().findUserById(caregiverId);
        user.enqueue(new Callback<UserDTO>() {
            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    Log.d("Caregiver id - found!", "onSuccess");
                    //find doctor by username

                    final UserDTO caregiverDTO = response.body();

                    final Call<UserDTO> user = UserService.getInstance().getUserByUsername(doctorUsername);
                    user.enqueue(new Callback<UserDTO>() {
                        @Override
                        public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                            if(response.isSuccessful()) {
                                Log.d("Doctor username", "onSuccess");

                                UserDTO doctorDTO = response.body();
                                //create the patient
                                System.out.println("birth day inauntru: "+userDTO.getBirthdate());
                                final PatientDTO patientDTO = new PatientDTO(
                                        medicalRecord,
                                        UserBuilder.generateEntityFromDTO(userDTO),
                                        UserBuilder.generateEntityFromDTO(caregiverDTO),
                                        UserBuilder.generateEntityFromDTO(doctorDTO)
                                );
                                System.out.println("birth day inauntru2 : "+userDTO.getBirthdate() + "- "+ patientDTO.getUser().getBirthdate());
                                //now add new patient to db

                                Call<Integer> newPatient = DoctorService.getInstance().insertPatientDTO(patientDTO);

                                newPatient.enqueue(new Callback<Integer>() {
                                    @Override
                                    public void onResponse(Call<Integer> call, Response<Integer> response) {
                                        if(response.isSuccessful()) {
                                            Log.d("Create new patient", "onSuccess" + patientDTO.getUser().getBirthdate());

                                            Toast.makeText(context, "Create new patient successful!", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Log.d("Create new patient", "failed");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Integer> call, Throwable t) {
                                        Log.d("Create new patient", "onFailure");
                                    }
                                });

                            } else {
                                Log.d("Doctor username", "failed");
                            }
                        }
                        @Override
                        public void onFailure(Call<UserDTO> call, Throwable t) {
                            Log.d("Doctor username", "onFailure");
                        }
                    });

                    } else {
                    Log.d("Caregiver id - found!", "failed");
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Caregiver id - found!", "onFailure");
             }
        });
    }

    public void getAllPatients(final Context context, final View currentView) {

        final Call<List<PatientDTO>> patientsList = DoctorService.getInstance().getAllPatients();

        patientsList.enqueue(new Callback<List<PatientDTO>>() {

            @Override
            public void onResponse(Call<List<PatientDTO>> call, Response<List<PatientDTO>> response) {
                if (response.isSuccessful()) {

                    List<PatientDTO> patientDTOS = new ArrayList<>();

                    for (PatientDTO u : response.body()) {
                        patientDTOS.add(u);
                    }

                    RecyclerView recyclerView = currentView.findViewById(R.id.recycler_view_patients);
                    PatientAdapter adapter = new PatientAdapter(patientDTOS, context);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(currentView.getContext()));

                } else {
                    Toast.makeText(context, "Get all patients failed! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PatientDTO>> call, Throwable t) {
                Log.d("Get all patients!", "onFailure");
            }
        });
    }


    public void deletePatientById(final Context context, final Integer patientId) {

        Call<ResponseBody> deleteRequest = DoctorService.getInstance().deletePatientById(patientId);
        deleteRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.d("Delete patient!", "onSuccess");
                    Toast.makeText(context, "Delete patient successful ! ", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("Delete patient!", "onFailure");
                    Toast.makeText(context, "Delete patient failed! ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Delete patient!", "onFailure");
                Toast.makeText(context, "Delete patient on failure! ", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void updatePatientMedicalRecord(final Context context, final Integer patientId, final String medicalRecord) {

        final Call<PatientDTO> patient = DoctorService.getInstance().findPatientById(patientId);
        patient.enqueue(new Callback<PatientDTO>() {
            @Override
            public void onResponse(Call<PatientDTO> call, Response<PatientDTO> response) {
                if (response.isSuccessful()) {
                    Log.d("Update patient record!", "onSuccess"+ response.body().getUser().getUsername());

                    PatientDTO updatedPatientDTO = response.body();
                    System.out.println("PATIENT:" + updatedPatientDTO.getUser().getUsername());
                    updatedPatientDTO.setMedicalRecord(medicalRecord);

                    Call<Integer> updatePatient = DoctorService.getInstance().updatePatientDTO(updatedPatientDTO);
                    updatePatient.enqueue(new Callback<Integer>() {
                        @Override
                        public void onResponse(Call<Integer> call, Response<Integer> response) {

                            if (response.isSuccessful()) {
                                Log.d("Update patient record", "onSuccess");
                                Toast.makeText(context, "Update patient medical record successful", Toast.LENGTH_SHORT).show();
                            } else {
                                Log.d("Update patient record", "onFailure");
                                Toast.makeText(context, "Update patient medical record failed", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Integer> call, Throwable t) {
                            Log.d("Update patient record", "onFailure");
                            Toast.makeText(context, "Update patient medical record on failure", Toast.LENGTH_SHORT).show();
                        }
                    });



                } else {
                    Log.d("Get patient ", "onFailure");
                    Toast.makeText(context, "Update patient medical record", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<PatientDTO> call, Throwable t) {
                Log.d("Get patient", "onFailure");
                Toast.makeText(context, "Update patient medical record", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //CRUD ON CAREGIVERS

    public void addNewCaregiver(final Context context, UserDTO userDTO) {

        Call<Integer> newCaregiverId = DoctorService.getInstance().insertCaregiverDTO(userDTO);

        newCaregiverId.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Creation successful!", Toast.LENGTH_SHORT).show();
                    System.out.println("caregiver id:"+response.body());
                } else {
                    Toast.makeText(context, "Creation failed! ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.d("Creation - caregiver!", "onFailure");
            }
        });
    }

    public void getAllCaregivers(final Context context, final View currentView) {

        final Call<List<UserDTO>> caregiversList = DoctorService.getInstance().getAllCaregivers();
        caregiversList.enqueue(new Callback<List<UserDTO>>() {

            @Override
            public void onResponse(Call<List<UserDTO>> call, Response<List<UserDTO>> response) {
                if (response.isSuccessful()) {
                    //put them in a recycler view

                    List<UserDTO> caregivers = new ArrayList<>();
                    System.out.println("ALL CAREGIVERS:");
                    for (UserDTO u : response.body()) {
                        System.out.println(u.getUsername());
                        caregivers.add(u);
                    }

                    System.out.println("size: "+ response.body().size());

                    RecyclerView recyclerView = currentView.findViewById(R.id.recycler_view_caregivers);
                    CaregiverAdapter adapter = new CaregiverAdapter(caregivers, context);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(currentView.getContext()));

                } else {
                    Toast.makeText(context, "Get all caregivers failed! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<UserDTO>> call, Throwable t) {
                Log.d("Get all caregivers!", "onFailure");
            }
        });
    }

    public void updateCaregiver(final Context context, final UserDTO caregiverDTO) {

        Call<UserDTO> user = UserService.getInstance().getUserByUsername(caregiverDTO.getUsername());
        user.enqueue(new Callback<UserDTO>() {

            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    System.out.println("userul:"+response.body().getUsername());
                    if( (response.body().getId() == caregiverDTO.getId()) && (response.body().getRole() == caregiverDTO.getRole())) {
                        System.out.println("Este userul cerut - VERIFICAT"+ response.body().getUsername());

                        Call<Integer> updateUser = DoctorService.getInstance().updateCaregiver(caregiverDTO);
                        updateUser.enqueue(new Callback<Integer>() {
                            @Override
                            public void onResponse(Call<Integer> call, Response<Integer> response) {
                                if(response.isSuccessful()) {
                                    Toast.makeText(context, "Update caregiver successful!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Update medication failed!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<Integer> call, Throwable t) {
                                Log.d("Update caregiver", "onFailure");
                                Toast.makeText(context, "Update caregiver onFailure!", Toast.LENGTH_SHORT).show();
                            }
                        });

                    }
                } else {
                    Log.d("Get user by username", "Fail");
                    Toast.makeText(context, "Get caregiver by username failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Get user by username", "onFailure");
                Toast.makeText(context, "Get caregiver by username onFailure!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void deleteCaregiverById(final Context context, final Integer caregiverId) {

        Call<UserDTO> user = UserService.getInstance().findUserById(caregiverId);
        user.enqueue(new Callback<UserDTO>() {

            @Override
            public void onResponse(Call<UserDTO> call, Response<UserDTO> response) {
                if (response.isSuccessful()) {
                    System.out.println("DELETE CAREGIVER"+ response.body().getUsername());
                    if(response.body().getRole() == Role.CAREGIVER) {

                        Call<ResponseBody> deleteRequest = DoctorService.getInstance().deleteCaregiverById(caregiverId);
                        deleteRequest.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful()) {
                                    Toast.makeText(context, "Delete caregiver successful!", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(context, "Delete caregiver failed! " + "\n" +
                                            "This caregiver has patients and it cannot be deleted!", Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Log.d("Delete caregiver!", "onFailure");
                                Toast.makeText(context, "Delete caregiver on failure! ", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } else {
                    Log.d("Get caregiver by id", "Fail");
                    Toast.makeText(context, "Get caregiver by id failed!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserDTO> call, Throwable t) {
                Log.d("Get caregiver by id", "onFailure");
                Toast.makeText(context, "Get caregiver by id onFailure!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    //CRUD ON MEDICATION


    public void createMedication(final Context context, MedicationDTO medicationDTO) {

        Call<Integer> newMedicationId = DoctorService.getInstance().insertMedicationDTO(medicationDTO);

        newMedicationId.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Creation successful!", Toast.LENGTH_SHORT).show();
                    System.out.println("medication id:"+response.body());
                } else {
                    Toast.makeText(context, "Creation failed! ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.d("Creation successful!", "onFailure");
            }
        });
    }

    public void getAllMedication(final Context context, final View currentView) {
        //AND PUT THEM ON RECYCLER VIEW

        final Call<List<MedicationDTO>> medicationList = DoctorService.getInstance().getAllMedications();

        medicationList.enqueue(new Callback<List<MedicationDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationDTO>> call, Response<List<MedicationDTO>> response) {
                if (response.isSuccessful()) {
                    //put them in a recycler view


                    RecyclerView recyclerView = currentView.findViewById(R.id.recycler_view);
                    MedicationAdapter adapter = new MedicationAdapter(response.body(), context);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(currentView.getContext()));

                } else {
                    Toast.makeText(context, "Get all medication failed! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<MedicationDTO>> call, Throwable t) {
                Log.d("Get all medication!", "onFailure");
            }
        });
    }

    public void updateMedication(final Context context, MedicationDTO medicationDTO) {

        Call<Integer> medicationId = DoctorService.getInstance().updateMedicationDTO(medicationDTO);
        medicationId.enqueue(new Callback<Integer>() {
            @Override
            public void onResponse(Call<Integer> call, Response<Integer> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Update medication successful!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Update medication failed! ", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Integer> call, Throwable t) {
                Log.d("Update medication!", "onFailure");
            }
        });
    }

    public void deleteMedicationById(final Context context, Integer medicationId) {
        Call<ResponseBody> deleteRequest = DoctorService.getInstance().deleteMedicationDTOById(medicationId);
        deleteRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Delete medication successful!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Delete medication failed! ", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("Delete medication!", "onFailure");
                Toast.makeText(context, "Delete medication on failure! ", Toast.LENGTH_SHORT).show();
            }
        });

    }

    //MEDICATION PLAN
    public void createMedicationPlan(final Context context, Integer patientId, final String startTime, final String endTime) {
        //1. find that patient by id

        Call<PatientDTO> patientDTOCall = DoctorService.getInstance().findPatientById(patientId);
        patientDTOCall.enqueue(new Callback<PatientDTO>() {
            @Override
            public void onResponse(Call<PatientDTO> call, Response<PatientDTO> response) {
                if(response.isSuccessful()) {
                    Log.d("Find patient by id!", "onSuccess");

                    PatientDTO currentPatient = response.body();
                    MedicationPlanDTO newMedPlan = new MedicationPlanDTO(
                            PatientBuilder.generateEntityFromDTO(currentPatient),
                            startTime,
                            endTime
                    );

                    Call<Integer> medPlanCall = DoctorService.getInstance().createMedicationPlan(newMedPlan);
                    medPlanCall.enqueue(new Callback<Integer>() {
                        @Override
                        public void onResponse(Call<Integer> call, Response<Integer> response) {
                            if(response.isSuccessful()) {
                                Log.d("Create medication plan", "onSuccess");
                                Toast.makeText(context, "Create medication plan successful! ", Toast.LENGTH_SHORT).show();

                            } else {
                                Log.d("Create medication plan", "failed");
                                Toast.makeText(context, "Create medication plan failed", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Integer> call, Throwable t) {
                            Log.d("Find patient by id!", "onSuccess");
                        }
                    });


                } else {
                    Log.d("Find patient by id!", "failed");
                }
            }

            @Override
            public void onFailure(Call<PatientDTO> call, Throwable t) {
                Log.d("Find patient by id!", "onFailure");
            }
        });

    }

    public void addMedicationToPlan(final Context context, Integer medicationPlanId, final MedicationDTO medicationDTO, final String intakeMoment) {
        //1. Get med plan by id
        //2. Add this medication to med plan calling backend server

        Call<MedicationPlanDTO> medicationPlanDTOCall = DoctorService.getInstance().getMedicationPlanById(medicationPlanId);
        medicationPlanDTOCall.enqueue(new Callback<MedicationPlanDTO>() {
            @Override
            public void onResponse(Call<MedicationPlanDTO> call, Response<MedicationPlanDTO> response) {
                Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan successful ");

                    MedicationPlanDTO medicationPlanDTO = response.body();
                    //create a med per plan dto
                    MedicationPerPlanDTO medicationPerPlanDTO = new MedicationPerPlanDTO(
                            intakeMoment,
                            MedicationBuilder.generateEntityFromDTO(medicationDTO),
                            MedicationPlanBuilder.generateEntityFromDTO(medicationPlanDTO)
                    );

                    //add the medperplan dto = adding a medication to a plan
                    Call<Integer> addMedToPlanCall = DoctorService.getInstance().addMedicationToPlan(medicationPerPlanDTO);
                    addMedToPlanCall.enqueue(new Callback<Integer>() {
                        @Override
                        public void onResponse(Call<Integer> call, Response<Integer> response) {
                            Log.d(TAG, "onResponse: addMedToPlanCall");
                            if (response.isSuccessful()) {
                                Log.d(TAG, "onResponse: addMedToPlanCall - successful");
                                Toast.makeText(context, "Add medication to plan - successful", Toast.LENGTH_SHORT).show();

//                                Intent resultIntent = new Intent(context, AddMedicationPlan.class);
//                                resultIntent.setResult(RESULT_OK, resultIntent);
//                                AddMedicationPlan.finish();

                            } else {
                                Log.d(TAG, "onResponse: addMedToPlanCall - failed");
                                Toast.makeText(context, "Add medication to plan - failed", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Integer> call, Throwable t) {
                            Log.d(TAG, "onFailure: addMedToPlanCall");
                        }
                    });

                } else {
                    Log.d(TAG, "onResponse: addMedicationToPlan - getMedPlan failed");
                }
            }

            @Override
            public void onFailure(Call<MedicationPlanDTO> call, Throwable t) {
                Log.d(TAG, "onFailure: addMedicationToPlan - getMedPlan");
            }
        });


    }

    /*
    public void showAllMedicationsForPlan(final Context context, Integer medicationPlanId, final View view) {

        Call<List<MedicationPerPlanDTO>> medicationPlanDTOCall = DoctorService.getInstance().getMedicationsForPlan(medicationPlanId);
        medicationPlanDTOCall.enqueue(new Callback<List<MedicationPerPlanDTO>>() {
            @Override
            public void onResponse(Call<List<MedicationPerPlanDTO>> call, Response<List<MedicationPerPlanDTO>> response) {
                Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan successful ");
                    //add this to recycler view
                    RecyclerView recyclerView = view.findViewById(R.id.recycler_doctor_med_plans);
                    MPMedicationAdapter adapter = new MPMedicationAdapter(response.body(), context);

                    recyclerView.setAdapter(adapter);
                    recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

                } else {
                    Log.d(TAG, "onResponse: getAllMedsFromPLan - getMedPlan failed");
                }
            }
            @Override
            public void onFailure(Call<List<MedicationPerPlanDTO>> call, Throwable t) {
                Log.d(TAG, "onFailure: getAllMedsFromPLan - getMedPlan");
            }
        });
    }*/
}
