package com.example.sd_android_project.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;


public class AddMedToPlanFragment extends Fragment {

    public static AddMedToPlanFragment newInstance() {

        Bundle args = new Bundle();

        AddMedToPlanFragment fragment = new AddMedToPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_med_to_plan, container, false);
    }

}
