package com.example.sd_android_project.views.fragments.patientsCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;

public class UpdateRecordPatientFragment extends Fragment {

    EditText patientId, medicalRecord;
    Button update;

    DoctorController doctorController = new DoctorController();

    public static UpdateRecordPatientFragment newInstance() {

        Bundle args = new Bundle();

        UpdateRecordPatientFragment fragment = new UpdateRecordPatientFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_update_patient_medinfo, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        patientId = view.findViewById(R.id.update_patient_medical_info_id);
        medicalRecord = view.findViewById(R.id.update_patient_medical_info_record);

        update = view.findViewById(R.id.update_patient_medical_record_button);

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateMedicalRecord(Integer.parseInt(patientId.getText().toString().trim()),
                                   medicalRecord.getText().toString().trim());
            }
        });
    }

    private void updateMedicalRecord(Integer id, String record) {
        doctorController.updatePatientMedicalRecord(this.getContext(), id, record);
    }
}
