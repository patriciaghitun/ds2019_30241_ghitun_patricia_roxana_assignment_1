package com.example.sd_android_project.dto.builders;

import com.example.sd_android_project.dto.user.UserDTO;
import com.example.sd_android_project.entities.User;

public class UserBuilder {

    private UserBuilder() {
    }

    public static UserDTO generateDTOFromEntity(User user) {
        return new UserDTO(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.getName(),
                user.getBirthdate(),
                user.getGender(),
                user.getAddress(),
                user.getRole());
    }

    public static User generateEntityFromDTO(UserDTO userDTO) {
        return new User(
                userDTO.getId(),
                userDTO.getUsername(),
                userDTO.getPassword(),
                userDTO.getName(),
                userDTO.getBirthdate(),
                userDTO.getGender(),
                userDTO.getAddress(),
                userDTO.getRole()
        );
    }

}
