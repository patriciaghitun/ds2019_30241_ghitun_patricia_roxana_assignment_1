package com.example.sd_android_project.dto.builders;


import com.example.sd_android_project.dto.medication.MedicationDTO;
import com.example.sd_android_project.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getSideEffects(),
                medication.getDosage());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO) {
        return new Medication(
                medicationDTO.getId(),
                medicationDTO.getName(),
                medicationDTO.getSideEffects(),
                medicationDTO.getDosage()
        );
    }
}
