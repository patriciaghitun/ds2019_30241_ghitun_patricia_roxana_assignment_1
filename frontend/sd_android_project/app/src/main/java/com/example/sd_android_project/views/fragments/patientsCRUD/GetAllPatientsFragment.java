package com.example.sd_android_project.views.fragments.patientsCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;
import com.example.sd_android_project.services.DoctorService;

public class GetAllPatientsFragment extends Fragment {

    DoctorController doctorController = new DoctorController();

    public static GetAllPatientsFragment newInstance() {

        Bundle args = new Bundle();

        GetAllPatientsFragment fragment = new GetAllPatientsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_getall_patients, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        doctorController.getAllPatients(this.getContext(), view);
    }
}
