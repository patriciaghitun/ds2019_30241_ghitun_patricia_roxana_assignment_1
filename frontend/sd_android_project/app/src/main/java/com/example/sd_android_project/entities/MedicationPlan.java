package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

public class MedicationPlan {

    @SerializedName("id")
    private Integer id;

    @SerializedName("patient_id")
    private Patient patient;

    @SerializedName("start")
    private String start;

    @SerializedName("end")
    private String end;

    public MedicationPlan(Integer id, Patient patient, String start, String end) {
        this.id = id;
        this.patient = patient;
        this.start = start;
        this.end = end;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
}
