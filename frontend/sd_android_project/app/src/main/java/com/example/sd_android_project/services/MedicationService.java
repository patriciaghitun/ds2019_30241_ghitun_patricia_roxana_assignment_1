package com.example.sd_android_project.services;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MedicationService {


    //private final static String API_URL = "http://192.168.0.101:8080/";
    //private final static String API_URL = "http://192.168.0.25:8080/";
    //private final static String API_URL = "http://192.168.0.100:8080/";
    //private final static String API_URL = "http://10.20.0.205:8080/";

    //private final static String API_URL = "http://192.168.0.103:8080/";
    //private final static String API_URL = "http://192.168.0.104:8080/";

    //private final static String API_URL = "http://192.168.0.103:8080/";

    private final static String API_URL = "http://192.168.1.96:8080/";
    private static MedicationApi medicationApi;

    //get la instanta de retrofit

    public static MedicationApi getInstance() {
        if (medicationApi == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(API_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(provideOkHttp())
                    .build();

            medicationApi = retrofit.create(MedicationApi.class);
        }
        return medicationApi;
    }

    private static OkHttpClient provideOkHttp() {
        OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();
        httpBuilder.connectTimeout(30, TimeUnit.SECONDS);

        return httpBuilder.build();
    }
}
