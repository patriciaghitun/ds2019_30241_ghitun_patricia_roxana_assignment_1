package com.example.sd_android_project.dto.builders;


import com.example.sd_android_project.dto.medication.MedicationPlanDTO;
import com.example.sd_android_project.entities.MedicationPlan;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO generateDTOFromEntity(MedicationPlan medicationPlan) {
        return new MedicationPlanDTO(
                medicationPlan.getId(),
                medicationPlan.getPatient(),
                medicationPlan.getStart(),
                medicationPlan.getEnd());
    }

    public static MedicationPlan generateEntityFromDTO(MedicationPlanDTO medicationPlanDTO) {
        return new MedicationPlan(
                medicationPlanDTO.getId(),
                medicationPlanDTO.getPatient(),
                medicationPlanDTO.getStart(),
                medicationPlanDTO.getEnd()
        );
    }
}
