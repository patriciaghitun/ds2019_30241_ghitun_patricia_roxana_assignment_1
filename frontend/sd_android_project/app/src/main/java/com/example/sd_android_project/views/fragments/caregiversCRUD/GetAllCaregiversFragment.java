package com.example.sd_android_project.views.fragments.caregiversCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;

public class GetAllCaregiversFragment extends Fragment {

    DoctorController doctorController = new DoctorController();

    public static GetAllCaregiversFragment newInstance() {

        Bundle args = new Bundle();

        GetAllCaregiversFragment fragment = new GetAllCaregiversFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_getall_caregivers, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        doctorController.getAllCaregivers(this.getContext(), view);
    }
}
