package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sd_android_project.R;

public class DoctorActivity extends AppCompatActivity {

    //PATIENTS
    private Button createPatientsButton, findAllPatientsButton, updatePatientButton, deletePatientButton;
    //CAREGIVERS
    private Button createCaregiverButton, findAllCaregiversButton, updateCaregiverButton, deleteCaregiverButton;
    //MEDICATION
    private Button addMedicationButton, findAllMedicationButton, updateMedicationButton, deleteMedicationButton;

    private Button createMedicationPlan;

    private ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //doctor ACTIVITY

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        Bundle extras = getIntent().getExtras();
        final String currentUsername = extras.getString("username");
        System.out.println("CURRENT USERNAME : "+ currentUsername);

        createPatientsButton = findViewById(R.id.button_create_patients);
        findAllPatientsButton = findViewById(R.id.button_findAll_patients);
        updatePatientButton = findViewById(R.id.button_update_patients);
        deletePatientButton = findViewById(R.id.button_delete_patients);

        createCaregiverButton = findViewById(R.id.button_create_caregivers);
        findAllCaregiversButton = findViewById(R.id.button_findAll_caregivers);
        updateCaregiverButton = findViewById(R.id.button_update_caregivers);
        deleteCaregiverButton = findViewById(R.id.button_delete_caregivers);

        addMedicationButton = findViewById(R.id.button_create_medication);
        findAllMedicationButton = findViewById(R.id.button_findAll_medication);
        updateMedicationButton = findViewById(R.id.button_update_medication);
        deleteMedicationButton = findViewById(R.id.button_delete_medication);

        createMedicationPlan = findViewById(R.id.button_create_medication_plan);

        //CRUD PATIENTS
        createPatientsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "createPatient");
                intent.putExtra("username", currentUsername);
                DoctorActivity.this.startActivity(intent);
            }
        });
        findAllPatientsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "getAllPatients");
                DoctorActivity.this.startActivity(intent);
            }
        });
        updatePatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "updatePatient");
                DoctorActivity.this.startActivity(intent);
            }
        });
        deletePatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "deletePatient");
                DoctorActivity.this.startActivity(intent);
            }
        });


        //CRUD CAREGIVERS
        createCaregiverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "createCaregiver");
                DoctorActivity.this.startActivity(intent);
            }
        });

        findAllCaregiversButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "getAllCaregivers");
                DoctorActivity.this.startActivity(intent);
            }
        });
        updateCaregiverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "updateCaregiver");
                DoctorActivity.this.startActivity(intent);
            }
        });
        deleteCaregiverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "deleteCaregiver");
                DoctorActivity.this.startActivity(intent);
            }
        });


        //CRUD MEDICATION
        addMedicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "createMedication");
                DoctorActivity.this.startActivity(intent);
            }
        });
        findAllMedicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "getAllMedication");
                DoctorActivity.this.startActivity(intent);
            }
        });
        updateMedicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "updateMedication");
                DoctorActivity.this.startActivity(intent);
            }
        });
        deleteMedicationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "deleteMedication");
                DoctorActivity.this.startActivity(intent);
            }
        });


        //MEDICATION PLAN
        createMedicationPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DoctorActivity.this, DoctorPatientsActivity.class);
                intent.putExtra("message", "createMedicationPlan");
                intent.putExtra("username", currentUsername);
                DoctorActivity.this.startActivity(intent);
            }
        });
    }
}
