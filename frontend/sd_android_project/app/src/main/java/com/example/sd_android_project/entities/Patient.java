package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Patient implements Serializable {

    @SerializedName("id")
    private Integer id;

    @SerializedName("medical_record")
    private String medicalRecord;

    @SerializedName("user_id")
    private User user;

    @SerializedName("caregiver_id")
    private User caregiver; // dozaj

    @SerializedName("doctor_id")
    private User doctor; // dozaj

    public Patient(Integer id, String medicalRecord, User user, User caregiver, User doctor) {
        this.id = id;
        this.medicalRecord = medicalRecord;
        this.user = user;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Patient(String medicalRecord, User user, User caregiver, User doctor) {
        this.medicalRecord = medicalRecord;
        this.user = user;
        this.caregiver = caregiver;
        this.doctor = doctor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCaregiver() {
        return caregiver;
    }

    public void setCaregiver(User caregiver) {
        this.caregiver = caregiver;
    }

    public User getDoctor() {
        return doctor;
    }

    public void setDoctor(User doctor) {
        this.doctor = doctor;
    }
}
