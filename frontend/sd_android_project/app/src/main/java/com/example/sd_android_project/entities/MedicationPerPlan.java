package com.example.sd_android_project.entities;

import com.google.gson.annotations.SerializedName;

public class MedicationPerPlan {

    @SerializedName("id")
    private Integer id;

    @SerializedName("intakemoment")
    private String intakemoment;

    @SerializedName("medication_id")
    private Medication medication;

    @SerializedName("medication_plan_id")
    private MedicationPlan medicationPlan;

    public MedicationPerPlan(Integer id, String intakemoment, Medication medication, MedicationPlan medicationPlan) {
        this.id = id;
        this.intakemoment = intakemoment;
        this.medication = medication;
        this.medicationPlan = medicationPlan;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIntakemoment() {
        return intakemoment;
    }

    public void setIntakemoment(String intakemoment) {
        this.intakemoment = intakemoment;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public MedicationPlan getMedicationPlan() {
        return medicationPlan;
    }

    public void setMedicationPlan(MedicationPlan medicationPlan) {
        this.medicationPlan = medicationPlan;
    }
}
