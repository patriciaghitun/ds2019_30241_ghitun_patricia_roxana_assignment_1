package com.example.sd_android_project.dto.builders;

import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO generateDTOFromEntity(Patient patient) {
        return new PatientDTO(
                patient.getId(),
                patient.getMedicalRecord(),
                patient.getUser(),
                patient.getCaregiver(),
                patient.getDoctor());
    }

    public static Patient generateEntityFromDTO(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getId(),
                patientDTO.getMedicalRecord(),
                patientDTO.getUser(),
                patientDTO.getCaregiver(),
                patientDTO.getDoctor()
        );
    }
}
