package com.example.sd_android_project.views.fragments.caregiversCRUD;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.controller.DoctorController;


public class DeleteCaregiverFragment extends Fragment {

    private EditText id;
    private Button delete;

    DoctorController doctorController = new DoctorController();

    public static DeleteCaregiverFragment newInstance() {

        Bundle args = new Bundle();

        DeleteCaregiverFragment fragment = new DeleteCaregiverFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delete_caregiver, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        id = view.findViewById(R.id.delete_caregiver_id);
        delete = view.findViewById(R.id.delete_caregiver);

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer medId = Integer.parseInt(id.getText().toString().trim());
                deleteCaregiverById(medId);
            }
        });
    }

    private void deleteCaregiverById(Integer id){
        doctorController.deleteCaregiverById(this.getContext(),id);
    }

}
