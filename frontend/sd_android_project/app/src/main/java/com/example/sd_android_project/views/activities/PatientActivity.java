package com.example.sd_android_project.views.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.user.PatientDTO;
import com.example.sd_android_project.services.PatientService;

import java.io.Serializable;

import javax.xml.datatype.Duration;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientActivity extends AppCompatActivity {
    private static final String TAG = "PatientActivity";

    TextView username, pass, name, gender, birthDate, address, medRecord, caregiverName;
    private Button toMedPlans;

    private PatientDTO patientDTO;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);

        Bundle extras = getIntent().getExtras();
        String currentUsername = extras.getString("username");
        System.out.println("CURRENT USERNAME : "+ currentUsername);



        username = findViewById(R.id.patient_username);

        //username.setText(currentUsername);

        pass = findViewById(R.id.patient_password);
        name = findViewById(R.id.patient_name);
        birthDate = findViewById(R.id.patient_birthDate);
        gender = findViewById(R.id.patient_gender);
        address = findViewById(R.id.patient_address);
        medRecord = findViewById(R.id.patient_medical_record);
        caregiverName = findViewById(R.id.patient_caregiver_name);

        getPatientData(currentUsername);


        toMedPlans = findViewById(R.id.patient_to_med_plans);
        toMedPlans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Patient - to med plans");
                Intent intent = new Intent(PatientActivity.this, PatientMedPlansActivity.class);
                intent.putExtra("username", username.getText().toString().trim());
                intent.putExtra("patientId",patientDTO.getId() );
                startActivity(intent);
            }
        });


    }

    public void getPatientData(final String usernameString) {
        //get patient by username
        Call<PatientDTO> patientDTOCall = PatientService.getInstance().findPatientByUsername(usernameString);
        patientDTOCall.enqueue(new Callback<PatientDTO>() {
            @Override
            public void onResponse(Call<PatientDTO> call, Response<PatientDTO> response) {
                Log.d(TAG, "onResponse: ");
                if(response.isSuccessful()) {
                    Log.d(TAG, "onResponse: succ");

                    Toast.makeText(getApplicationContext(), "Login successful", Toast.LENGTH_LONG).show();

                    patientDTO = response.body();
                    System.out.println(patientDTO.getUser().getName());

                    username.setText(patientDTO.getUser().getUsername());
                    pass.setText(patientDTO.getUser().getPassword());
                    name.setText(patientDTO.getUser().getName());
                    gender.setText(patientDTO.getUser().getGender());
                    birthDate.setText(patientDTO.getUser().getBirthdate());
                    address.setText(patientDTO.getUser().getAddress());

                    medRecord.setText(patientDTO.getMedicalRecord());

                    caregiverName.setText(patientDTO.getCaregiver().getName());

                } else {
                    Log.d(TAG, "onResponse: failed");
                }
            }

            @Override
            public void onFailure(Call<PatientDTO> call, Throwable t) {
                Log.d(TAG, "onFailure: ");
                Toast.makeText(getApplicationContext(), "Could not load patient data!", Toast.LENGTH_LONG).show();
            }
        });
    }

}
