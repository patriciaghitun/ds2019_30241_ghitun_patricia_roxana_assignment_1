package com.example.sd_android_project.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sd_android_project.R;
import com.example.sd_android_project.dto.medication.MedicationDTO;

import java.util.ArrayList;
import java.util.List;

public class MedicationAdapter extends RecyclerView.Adapter<MedicationAdapter.MedicationViewHolder>{

    private static final String TAG = "MedicationAdapter";

    private List<MedicationDTO> medicationDTOS = new ArrayList<>();
    private Context context ;

    public MedicationAdapter(List<MedicationDTO> medicationDTOS, Context context) {
        this.medicationDTOS = medicationDTOS;
        this.context = context;
    }

    @NonNull
    @Override
    public MedicationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.allmeds_recycler_item, parent, false);

        MedicationViewHolder medicationViewHolder = new MedicationViewHolder(view);
        return medicationViewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull MedicationViewHolder holder, final int position) {


        holder.medName.setText(medicationDTOS.get(position).getName());
        holder.medSideEffects.setText(medicationDTOS.get(position).getSideEffects());
        holder.medDosage.setText(medicationDTOS.get(position).getDosage().toString());

        holder.allMedsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"Medicamentul:" + medicationDTOS.get(position).getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return medicationDTOS.size();
    }


    public class MedicationViewHolder extends RecyclerView.ViewHolder {

        TextView medName;
        TextView medSideEffects;
        TextView medDosage;

        LinearLayout allMedsLayout;

        public MedicationViewHolder(@NonNull View itemView) {
            super(itemView);

            medName = itemView.findViewById(R.id.recycler_med_name);
            medSideEffects = itemView.findViewById(R.id.recycler_med_sideeffects);
            medDosage = itemView.findViewById(R.id.recycler_med_dosage);

            allMedsLayout = itemView.findViewById(R.id.allmeds_layout);
        }
    }

}
