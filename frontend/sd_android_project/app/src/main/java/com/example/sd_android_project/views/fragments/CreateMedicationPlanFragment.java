package com.example.sd_android_project.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.sd_android_project.R;
import com.example.sd_android_project.views.fragments.patientsCRUD.UpdateRecordPatientFragment;

public class CreateMedicationPlanFragment extends Fragment {

    EditText patientId, startTime, endTime;
    Button save, addMedication;

    public static CreateMedicationPlanFragment newInstance() {

        Bundle args = new Bundle();

        CreateMedicationPlanFragment fragment = new CreateMedicationPlanFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_create_medication_plan, container, false);
    }

//
//    @Override
//    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
//        super.onViewCreated(view, savedInstanceState);
//        System.out.println("Create medication plan main screen");
//
//        patientId = view.findViewById(R.id.mp_patient_id);
//        startTime = view.findViewById(R.id.mp_start_time);
//        endTime = view.findViewById(R.id.mp_end_time);
//
//        addMedication = view.findViewById(R.id.add_medication_to_plan);
//
//        addMedication.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println("add medication to medication plan tapped");
//                addMedicationToMedicationPlan();
//            }
//        });
//
//        save = view.findViewById(R.id.save_medication_plan);
//
//    }
//
//    private void addMedicationToMedicationPlan() {
//        // go to add m to m plan fragment with my info
//        // frag args : medication
//
//
//        this.getFragmentManager().beginTransaction()
//                .add(R.id.doctor_patient_frame, AddMedicationToMedicationPlanFragment.newInstance())
//                .commit();
//
//    }
}
